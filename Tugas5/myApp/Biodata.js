import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';

export default class Biodata extends Component{
    render(){
        return(
            <View style = {styles.container}>
                <View style={{marginTop:64, alignItems:'center'}}>
                    <View style = {styles.avatarcontainer}>
                        <Image style={styles.avatar} source = {require('./assets/ringga.jpg')} />
                    </View>
                    <Text style={styles.biodata}>BIODATA</Text>
                </View>
                <View style={styles.statsContainer}>
                    <View style={styles.stat}>
                        <Text style={styles.statAmount}>Instragram</Text>
                        <Text style={styles.statTitle}>ringgsyhbdn</Text>
                    </View>
                    <View style={styles.stat}>
                        <Text style={styles.statAmount}>Facebook</Text>
                        <Text style={styles.statTitle}>Ringga Syihabbudin</Text>
                    </View>
                    <View style={styles.stat}>
                        <Text style={styles.statAmount}>Twitter</Text>
                        <Text style={styles.statTitle}>@ringga17</Text>
                    </View>
                </View>
                <View style={styles.nama}>
                    <Text style={styles.name}>Nama                     :      Ringga Syihabbudin </Text>
                </View>
                <View style={styles.status}>
                    <Text style={styles.study}>Program study    :      Teknik Informatika</Text>
                </View>
                <View style={styles.class}>
                    <Text style={styles.kelas}>kelas                      :       Pagi A</Text>
                </View>
                
            </View>
        )
    }
} 
const styles = StyleSheet.create({
    container:{
        backgroundColor: '#020828',
        flex:1,
        width: 500
    },
    avatarcontainer: {
        shadowColor:'#FFFFFF',
        shadowRadius: 15,
        shadowOpacity: 0.4
    },
    avatar: {
        width: 136,
        height: 136,
        borderRadius: 68
    },
    biodata: {
        color: '#FFFFFF',
        marginTop: 24,
        fontSize: 20,
        fontWeight: 'bold'
    },
    statsContainer: {
        flexDirection: "row",
        justifyContent: 'space-between',
        margin: 50,
        bottom: 20
    },
    stat: {
        alignItems: 'center',
        flex : 1
    },
    statAmount:{
        color:'#FFFFFF',
        fontSize: 18,
        fontWeight: 'bold'
    },
    statTitle: {
        fontSize: 16,
        color: '#FFFFFF',
        fontWeight: '300',
        marginTop: 4
    },
    nama: {
        color: '#FFFFFF',
        bottom: 14,
        left: 60
    },
    name:{
        justifyContent: 'flex-end',
        color: '#FFFFFF',
        fontSize: 18,
        fontWeight: 'bold',
    },
    status: {
        color: '#FFFFFF',
        bottom:-20,
        left: 60
    },
    study:{
        justifyContent: 'flex-end',
        color: '#FFFFFF',
        fontSize: 18,
        fontWeight: 'bold',
        
    },
    class: {
        color: '#FFFFFF',
        bottom : -50,
        left : 60
    },
    kelas: {
        justifyContent: 'flex-end',
        color: '#FFFFFF',
        fontSize: 18,
        fontWeight: 'bold',
    },
    state : {
        color: '#FFFFFF',
        fontSize : 18,
        left : 60,
        bottom :-15
    }
});