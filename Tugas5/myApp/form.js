import React, {Component} from 'react';
import { StyleSheet, Text, View, TextInput, Image, TouchableOpacity } from 'react-native';

export default class Logo extends Component {
    render(){
        return(
            
            <View style = {styles.container}>
                <TextInput style={styles.inputBox} underlineColorAndroid='rgba(0,0,0,0)'
                 placeholder='Username/email' 
                 placeholderTextColor='#ffffffff'
                 />
                 <TextInput style={styles.inputBox} underlineColorAndroid='rgba(0,0,0,0)'
                 placeholder='Password'
                 secureTextEntry={true} 
                 placeholderTextColor='#ffffffff'
                 />
                 <TouchableOpacity style={styles.button}>
                     <Text style={styles.buttonText}>Login</Text>
                 </TouchableOpacity>
                 <TouchableOpacity style={styles.loginwith}>
                     <Text style={styles.loginwithText}>or login with</Text>
                 </TouchableOpacity>
                 <TouchableOpacity style={styles.facebook}>
                     <Text style={styles.loginwithfacebook}>Facebook</Text>
                 </TouchableOpacity>
                 <TouchableOpacity style={styles.instagram}>
                     <Text style={styles.loginwithinstagram}>Instragram</Text>
                 </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
          flexGrow: 1,
          justifyContent: 'center',
          alignItems: 'center'
        },
    inputBox:{
        width:300,
        height:50,
        backgroundColor: 'rgba(255, 255, 255, 0.3)',
        borderRadius: 25,
        paddingHorizontal:15,
        fontSize: 16,
        color:'#fff',
        marginVertical: 9
    },
    button:{
        width: 150,
        height:40,
        backgroundColor: '#000000',
        borderRadius:25,
        marginVertical:10,
        paddingVertical:13
    },
    buttonText: {
        fontSize:16,
        fontWeight:'500',
        color:'#fff',
        textAlign:'center'
    },
    
    loginwithText: {
        fontSize:16,
        fontWeight:'500',
        color:'#fff',
        textAlign:'center'
    },
    facebook:{
        width: 200,
        height:40,
        backgroundColor: 'blue',
        borderRadius:25,
        marginVertical:10,
        paddingVertical:13
    },
    loginwithfacebook: {
        fontSize:16,
        fontWeight:'500',
        color:'#fff',
        textAlign:'center'
    },
    instagram:{
        width: 200,
        height:40,
        backgroundColor: '#FF0099',
        borderRadius:25,
        marginVertical:10,
        paddingVertical:13
    },
    loginwithinstagram: {
        fontSize:16,
        fontWeight:'500',
        color:'#fff',
        textAlign:'center'
    },
});
